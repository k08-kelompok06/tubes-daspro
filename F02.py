import konversiCSVkeArray,tools

arrayUser = konversiCSVkeArray.CSVUserTOArray()


def register():
    global arrayUser
    nama = input("Masukan nama: ")
    username = input("Masukkan username: ")
    password = input("Masukkan password: ")


    for i in range(tools.panjangArray(arrayUser)):
        if arrayUser[i][1] == username:
            print(f"Username {username} sudah terpakai, silahkan menggunakan username lain.")
            return arrayUser
    
    validUsername = False
    for char in username:
        if (ord(char) == 45 or ord(char) == 95 or (ord(char)>= 65 and ord(char)<=90) or (ord(char) >= 97 and ord(char)<= 122) or (48 <= ord(char) <= 57)):
            validUsername = True
    
    if validUsername == False:
        print(f"Username {username} tidak valid!")
    else:
        arrayUser = arrayUser + [[str(tools.panjangArray(arrayUser)), username, nama, password, "User", "0"]]
        print(f'Username {username} telah berhasil register ke dalam "Binomo"')
        
    return arrayUser

register()
print(register())