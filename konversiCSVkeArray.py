def CSVUserTOArray():
    global arrayUser
    f = open("user.csv", "r")
    lines = f.readlines()
    f.close()

    arrayUser = []

    for line in lines:
        n = 0
        line_data = ['']
        for char in line:
            if char == ';':
                line_data += ['']
                n += 1
            elif char != '\n':
                line_data[n] += char

        arrayUser += [line_data]

    return arrayUser

def CSVGameTOArray():
    global arrayGame
    f = open("game.csv", "r")
    lines = f.readlines()
    f.close()

    arrayGame = []

    for line in lines:
        n = 0
        line_data = ['']
        for char in line:
            if char == ';':
                line_data += ['']
                n += 1
            elif char != '\n':
                line_data[n] += char

        arrayGame += [line_data]

    return arrayGame

CSVUserTOArray()
