#tictactoe

def cetakPapan(arr):
    for k in range(3):
        for j in range(3):
            print(arr[k][j],end="")
        print()

def winner(arr,character):
    state = False
    if (arr[0][0] == arr[0][1] == arr[0][2] == character):
        state = True
    elif (arr[1][0] == arr[1][1] == arr[1][2] == character):
        state = True
    elif (arr[2][0] == arr[2][1] == arr[2][2] == character):
        state = True
    elif (arr[0][0] == arr[1][0] == arr[2][0] == character):
        state = True
    elif (arr[0][1] == arr[1][1] == arr[2][1] == character):
        state = True
    elif (arr[0][2] == arr[1][2] == arr[2][2] == character):
        state = True
    elif (arr[0][0] == arr[1][1] == arr[2][2] == character):
        state = True
    elif (arr[2][0] == arr[1][1] == arr[0][2] == character):
        state = True
    return state
    
    
    
def tictactoe():
    print("Legenda: ")
    print("# Kosong")
    print("X Pemain 1")
    print("O Pemain 2")

    papan = [["#" for i in range(3)] for j in range(3)]

    state = winner(papan,"X")
    pemenangX = False
    pemenangY = False
    counter = 1

    while counter <= 9 and not(state):

        if counter % 2 == 1:
            print('Giliran pemain "X": ')
            state1 = True

            while state1: #Lakukan validasi input
                x = int(input("x: "))
                y = int(input("y: "))

                if (1 <= x <= 3) and (1<= y <= 3) and (papan[x-1][y-1] != "X") and (papan[x-1][y-1] != "Y"):
                    state1 = False #Input tervalidasi
                    papan[x-1][y-1] = "X"
                    pemenangX = winner(papan,"X")
                elif not(1 <= x <= 3) or not(1<= y <= 3): #Lakukan validasi hingga benar
                    print("Masukkan tidak valid!")
                else:
                    print("Kotak sudah terisi!")
            state = pemenangX

        else:
            print('Giliran pemain "Y": ')
            state2 = True

            while state2: #Lakukan validasi input
                x = int(input("x: "))
                y = int(input("y: "))

                if (1 <= x <= 3) and (1<= y <= 3) and (papan[x-1][y-1] != "Y") and (papan[x-1][y-1] != "X"):
                    state2 = False #Input tervalidasi
                    papan[x-1][y-1] = "Y"
                    pemenangY = winner(papan,"Y")

                elif not(1 <= x <= 3) or not(1<= y <= 3): #Lakukan validasi hingga benar
                    print("Masukkan tidak valid!")
                elif papan[x-1][y-1] == "X":
                    print("Kotak sudah terisi!")
            state = pemenangY

        #Cetak status papan
        cetakPapan(papan)
        counter += 1

    if pemenangX:
        print('Pemain "X" menang.')
    elif pemenangY:
        print('Pemain "Y" menang.')
    else:
        print("Hasil permainan seri")


