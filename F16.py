import csv
import konversiCSVkeArray,tools
import os

def Save(sv,name_folder,arrayUser,arrayGame):
    os.chdir(os.getcwd()+str('/')+ str(name_folder))
    pathnow = os.getcwd()+str('/')+ str(name_folder)
    for path,folder,file in os.walk(pathnow):
        for data in file:
            if data == []:
                break
            os.remove('game1.csv')
            os.remove('user1.csv')

    if sv == 'Y' or 'y':

        with open('user1.csv', 'a') as csvfile:
            fields = ['id','username','nama','password','role','saldo']
            writer = csv.DictWriter(csvfile, fieldnames = fields, delimiter = ';')
            
            writer.writeheader()
            for i in range(tools.panjangArray(arrayUser)-1):
                writer.writerow({
                    'id' : arrayUser[i+1][0],
                    'username' : arrayUser[i+1][1],
                    'nama' : arrayUser[i+1][2],
                    'password' : arrayUser[i+1][3],
                    'role' : arrayUser[i+1][4],
                    'saldo' : arrayUser[i+1][5],
                })
                

        with open('game1.csv', 'a') as csvfile:
            fields = ['id','nama','kategori','tahun_rilis','harga','stok']
            writer = csv.DictWriter(csvfile, fieldnames = fields, delimiter = ';')
            
            writer.writeheader()
            for i in range (tools.panjangArray(arrayGame)-1):
                writer.writerow({
                    'id' : arrayGame[i+1][0],
                    'nama' : arrayGame[i+1][1],
                    'kategori' : arrayGame[i+1][2],
                    'tahun_rilis' : arrayGame[i+1][3],
                    'harga' : arrayGame[i+1][4],
                    'stok' : arrayGame[i+1][5]
                })

if __name__ == '__main__':
    name_folder = str(input("Masukkan nama folder penyimpanan: "))
    pathnow = os.getcwd()
    for path, folder, file in os.walk(pathnow):
        for i in range (tools.panjangArray(folder)):
            exist_folder = False
            if folder[i] == name_folder:
                exist_folder = True
                break
        if exist_folder == False:
            os.mkdir(name_folder)
            break
    print("Data telah disimpan pada folder",name_folder)
    import F17
    sv = F17.exit()

    arrayUser = konversiCSVkeArray.CSVUserTOArray()
    arrayGame = konversiCSVkeArray.CSVGameTOArray()

    Save(sv,name_folder,arrayUser,arrayGame)