
def length(X):
    global length
    l = 0
    for a in (X):
        l += 1
    return l

def convert_int(X):
    global convert_int
    num = ""
    for a in (X):
        if 48 <= ord(a) <= 57:
            num += a
    return int(num)

def bubblesort(arr):
    global bubblesort
    l = length(arr)
    for i in range(l-1):
        for k in range(l-1,i,-1):
            if (arr[k] < arr[k-1]):
                temp = arr[k]
                arr[k] = arr[k-1]
                arr[k-1] = temp
    return arr

def sort_descending(arr):
    global sort_descending
    arr = bubblesort(arr)
    temp = []
    for i in range(length(arr)):
        temp += [arr[length(arr)-(i+1)]]  
    arr = temp
    return arr


## CSV read and write

### 1. CSV Read ###
def splitting(line,n): #fungsi untuk mengubah string menjadi array of string
    #line merupakan string yang akan dipisah dalam file csv
    #n merupakan jumlah string yang dipisahkan dengan ";"
    global splitting
    l = length(line)
    counter = 0
    arr_of_line = ["" for i in range(n)]
    for i in range(n):
        state = True
        while state and counter <= (l-2):
            if line[counter] != ";":
                arr_of_line[i] += line[counter]
                counter += 1
            else:
                counter += 1
                state = False
    return arr_of_line

def read_csv(filename,n): #membaca file csv(filename) dan jumlah kolom efektif(n)
    #fungsi akan mereturn array of string yang akan disimpan di python dan diolah
    #contoh filename: game.csv
    global read_csv
    counter = -1
    arr = []
    with open(filename,"r") as s:
        for line in s:
            counter += 1
            if counter >= 1: #header file diskip
                arr += [splitting(line,n)]
    return arr

def write_csv(filename,header,arr): #menulis file dengan input nama file yang dimaksud, header file
    #serta arr sebagai data yang disimpan dalam array of string untuk ditulis pada file csv
    #contoh filename: game.csv
    #contoh header: "nama;id;tahun_terbit;harga"
    global write_csv

    f = open(filename,"w")

    f.writelines(f"{header}\n")
    for i in range(len(arr)):
        for j in range(len(arr[i])):
            if j != len(arr[i])-1:
                f.writelines(f"{arr[i][j]};")
            else:
                f.writelines(f"{arr[i][j]}\n")


