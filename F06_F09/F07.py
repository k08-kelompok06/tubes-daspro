from .function import *

#F07
def ascsort_arrofgame(arr_game,n): #Gunakan algoritma bubble sort untuk sorting arr of game
    arrsort = arr_game
    l = length(arrsort)
    for i in range(l-1):
        for k in range(l-1,i,-1):
            if convert_int(arrsort[k][n]) < convert_int(arrsort[k-1][n]):
                temp = arrsort[k]
                arrsort[k] = arrsort[k-1]
                arrsort[k-1] = temp
    return arrsort

def print_template(arr,n): #template untuk print arr of game
    print(f"{n+1}. {arr[n][0]} | {arr[n][1]} | {arr[n][2]} | {arr[n][3]} | {arr[n][4]} | {arr[n][0]} \n")

def list_game_toko(arr_game):
    skema = input("Skema sorting: ")
    if skema.lower() == "tahun+": 
        #sorting berdasarkan tahun(ascending). Tahun berada pada indeks ke-3
        arr_sort = ascsort_arrofgame(arr_game,3)
        for i in range(length(arr_sort)):
            print_template(arr_sort,i)

    elif skema.lower() == "tahun-":
        #sorting berdasarkan tahun(descending). Tahun berada pada indeks ke-3
        arr_sort = ascsort_arrofgame(arr_game,3)
        for i in range(length(arr_sort)-1,-1,-1):
            print_template(arr_sort,i)

    elif skema.lower() == "harga+":
        #sorting berdasarkan harga(ascending). Harga berada pada indeks ke-4
        arr_sort = ascsort_arrofgame(arr_game,4)
        for i in range(length(arr_sort)):
            print_template(arr_sort,i)

    elif skema.lower() == "harga-":
    #sorting berdasarkan harga(descending). Tahun berada pada indeks ke-4
        arr_sort = ascsort_arrofgame(arr_game,4)
        for i in range(length(arr_sort)-1,-1,-1):
            print_template(arr_sort,i)
    
    else: #Skema sorting tak valid
        print("Skema sorting tidak valid!")