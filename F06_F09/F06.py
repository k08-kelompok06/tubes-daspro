from .function import *

#F06
def ubah_stok(arr_game):
    id_game = input("Masukkan ID Game: ")
    
    state = True

    for _ in arr_game:
        if _[0] == id_game:
            n = int(input("Masukkan jumlah: "))
            stok_sekarang = convert_int(_[5]) + n
            if (stok_sekarang) >= 0:
                _[5] = str(stok_sekarang)
                if n >= 0: 
                    print(f"Stok game {_[1]} berhasil ditambahkan. Stok sekarang: {stok_sekarang} \n")
                else:
                    print(f"Stok game {_[1]} berhasil dikurangi. Stok sekarang: {stok_sekarang} \n")
            else:
                print(f"Stok game {_[1]} gagal dikurangi karena stok kurang. Stok sekarang: {stok_sekarang-n} \n")
            state = False
        
    if state:
        print("Tidak ada game dengan ID tersebut!")