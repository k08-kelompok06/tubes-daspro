from .function import *

#F08

#prosedur buy_game
def buy_game(user_id,arr_user,arr_game,kepemilikan): #hanya dapat diakses oleh user, bukan admin
    #KAMUS LOKAL
    #user_id: string
    #game: array of string
    #kepemilikan: array of string

    game_id = input("Masukkan ID Game: ")
    
    #lakukan skema validasi
    state0 = False
    user = ""
    
    for x in arr_game:
        if game_id == x[0]:
            game = x #menemukan tipe game yang sesuai
            state0 = True #Ditemukan ID game yang sesuai
    
    for y in arr_user:
        if user_id == y[0]:
            user = y #menemukan user yang sesuai

    
    #Lakukan skema validasi kembali
    if state0:
        #lakukan skema validasi kembali dengan meninjau kepemilikan, stok game,
        #dan saldo user
        state1 = False

        for y in kepemilikan: #lakukan validasi untuk mencek kepemilikan
            if y[0] == user_id:
                if y[1] == game[0]:
                    state1 = True
        
        if state1:
            print("Anda sudah memiliki game tersebut!")
        elif int(game[5]) == 0: #lakukan validasi stok game terlebih dahulu
            print("Stok game tersebut sedang habis")

        elif int(user[5]) >= convert_int(game[4]): #user masih punya saldo
            print(f'Game "{game[1]}" berhasil dibeli!') 
            #lakukan perubahan pada user dan game
            harga_game = convert_int(game[4])

            #UPDATE ARRAY OF USER
            counter = 0
            for i in arr_user: #Lakukan pengurangan saldo
                if i[0] == user_id:
                    arr_user[counter][5] =  str(int(arr_user[counter][5]) - harga_game)
                counter += 1

            #UPDATE ARRAY OF GAME
            counter = 0
            for y in arr_game: #Lakukan pengurangan stok
                if game_id == y[0]:
                    arr_game[counter][5] = str(int(y[5]) - 1)
                counter += 1

            #UPDATE KEPEMILIKAN
            kepemilikan += [[user_id,game_id]]

        else: #user tidak cukup saldo untuk membeli game
            print("Saldo anda tidak cukup untuk membeli Game tersebut!")

    else:
        print("Tidak ada Game dengan ID tersebut")

#Contoh implementasi: buy_game("123",arr_of_user,arr_of_game,arr_of_kepemilikan)