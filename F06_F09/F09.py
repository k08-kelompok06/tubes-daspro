from .function import *

#F09
def list_game(kepemilikan,user_id,arr_game): #Asumsi saat menjalankan subprogram, akses sebagai user dengan user_id
    #KAMUS LOKAL
    #kepemilikan: array of string
    #user_id : integer
    #counter: integer
    #arr: array of integer(menampung game ID)

    #ALGORITMA
    state = True
    #Lakukan validasi
    counter = 0
    arr_ID = []

    for z in kepemilikan:
        if z[1] == user_id:
            arr_ID += [z[0]] #tambahkan game ID
            state = False #User memiliki game
        counter += 1

    if state: #User tak memiliki game
        print("Maaf, kamu belum membeli game. Ketik perintah beli_game untuk beli.")
    else:
        l_arr_game = length(arr_game)
        l_arr_ID = length(arr_ID)

        #Melakukan looping untuk mencetak (nested loop)
        counter = 1
        for i in range(l_arr_game):
            for j in range(l_arr_ID):
                if arr_game[i][0] == arr_ID[j]:
                    print(f"{counter}. {arr_game[i][0]} | {arr_game[i][1]} | {arr_game[i][2]} | {arr_game[i][3]} | {arr_game[i][4]}")
                    counter += 1

#Contoh implementasi: list_game(arr_of_kepemilikan,"123",arr_of_game)