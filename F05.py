import konversiCSVkeArray, tools

arrayGame = konversiCSVkeArray.CSVGameTOArray()

def ubahGame(arrayGame):
    ubah_id = input("Masukkan ID game: ")
    ubah_nama = input("Masukkan nama game: ")
    ubah_kategori = input("Masukkan kategori: ")
    ubah_tahun_rilis = input("Masukkan tahun rilis: ")
    ubah_harga = input("Masukkan harga: ")
    
    ubahGameValid = True
    if ubah_id == "" :
        ubahGameValid == False
    
    if ubahGameValid == False:
        print("Masukkan ID game!")
        ubahGame()
    else :
        for i in range(tools.panjangArray(arrayGame)):
            if arrayGame[i][0] == ubah_id :
                if ubah_nama != "" :
                    arrayGame[i][1] = ubah_nama
                if ubah_kategori != "" :
                    arrayGame[i][2] = ubah_kategori
                if ubah_tahun_rilis != "" :
                    arrayGame[i][3] = ubah_tahun_rilis
                if ubah_harga != "" :
                    arrayGame[i][4] = ubah_harga
    return arrayGame

print(ubahGame(arrayGame))