import argparse
import konversiCSVkeArray
import F14

parser = argparse.ArgumentParser(description='Loading Binomo')
group = parser.add_mutually_exclusive_group()
group.add_argument("-g", '--game', action='store_true', help="Untuk membuka file game")
group.add_argument("-u", '--user', action='store_true', help="Untuk memperoleh file user")
group.add_argument("-r", '--riwayat', action='store_true', help="Untuk memperoleh data riwayat game")
group.add_argument("-k", '--kepemilikan', action='store_true', help="Untuk memperoleh data kepemilikan")
group.add_argument("-H", '--help', action='store_true', help="Untuk membuka panduan aplikasi")
args = parser.parse_args()

if __name__ == '__main__':
    if args.game:
        print(konversiCSVkeArray.CSVGameTOArray())
    elif args.user:
        print(konversiCSVkeArray.CSVUserTOArray())
    elif args.help:
        F14
    
        